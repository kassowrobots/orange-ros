# ROS for Kassow Robots

## Overview

ROS for Kassow Robots allows you to monitor and control all KR models via the Robot Operating System (ROS). For using the ROS with real robot you need to install the ROS Interface CBun. See our wiki for detailed description. 

## Installation

ROS for Kassow Robots requires Linux setup with ROS.  We recommend ROS Melodic and Ubuntu 18.04. Although it was tested also against OS X (High Sierra) with ROS Melodic, it is not oficially supported.  

```bash
cd ~/catkin_ws/src
git clone https://gitlab.com/kassowrobots/orange-ros.git
cd ~/catkin_ws
catkin_make --pkg=kr_msgs install
catkin_make --pkg=kr_example_python install
```


After building kr_msgs package, there is one more important step to do on your laptop (desktop). Set environment variables to its proper values. Since the ROS master will be running on your desktop, the ROS_MASTER_URI, ROS_HOSTNAME and ROS_IP variables should contain the IP address of the laptop (desktop) network interface (the one that is being used for the communication with the robot). These adresses should also correspond to the IP address and Port specified in the CBun activation params. Example bash commands can be seen below:

```bash
export ROS_MASTER_URI=http://192.168.100.138:11311
export ROS_HOSTNAME=192.168.100.138
export ROS_IP=192.168.100.138
```


## Usage

Requires ROS Control from ROS Interface CBun running on Kassow Robot and roscore running on your local computer. Following examples also require "kr" as CBun Control Namespace.

### kr_msgs

#### System State

```bash
rostopic echo /kr/system/state
```
#### Jogging

```bash
rostopic pub -r 20 /kr/motion/jog_joint kr_msgs/JogJoint "jsvel: [30.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]" 
```
#### Services

```bash
rosservice call /kr/iob/set_digital_output 1 1
```

### kr_example

#### state_subscriber

```bash
rosrun kr_example_python state_subscriber.py
```

#### iob_control

```bash
rosrun kr_example_python iob_control.py
```

#### ps4_jogging

```bash
rosrun kr_example_python ps4_jogging.py
```


cmake_minimum_required(VERSION 2.8.3)
project(kr_example_python)

find_package(catkin REQUIRED COMPONENTS
  rospy
  kr_msgs  
  sensor_msgs
)

###################################
## catkin specific configuration ##
###################################
catkin_package(
#  INCLUDE_DIRS include

  #  LIBRARIES basics
#  CATKIN_DEPENDS rospy
#  DEPENDS system_lib
#  CATKIN_DEPENDS actionlib_msgs
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

#############
## Install ##
#############

install(PROGRAMS src/state_subscriber.py
   DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(PROGRAMS src/iob_control.py
   DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(PROGRAMS src/ps4_jogging.py
   DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
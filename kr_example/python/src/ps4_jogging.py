#!/usr/bin/env python2.7

import rospy

from sensor_msgs.msg import Joy
from kr_msgs.msg import *


jjs_pub = None
jsm_pub = None
btn_pub = None


def thread_subscriber():
    rospy.Subscriber("joy", Joy, joy_cb, queue_size=1)
    rospy.spin()


def joy_cb(msg):
    global jjs_pub
    global jsm_pub
    global btn_pub
    
    dsm = 0.0
    dpos = [0.0, 0.0, 0.0]
    
    # left arrow
    if msg.buttons[14]:
        dpos[1] = 150
    # top arrow
    elif msg.buttons[15]:
        dpos[0] = 150
    # right arrow
    elif msg.buttons[16]:
        dpos[1] = -150
    # bottom arrow
    elif msg.buttons[17]:
        dpos[0] = -150
    else:
        # L2/R2 controls self motion
        if msg.axes[4] != 0 or msg.axes[5] != 0:
            dsm = (-1.0 * msg.axes[4]) + (msg.axes[5])  # norm self-motion speed
        
        # left joy controls motion in X and Y, right joy controls motion in Z
        elif msg.axes[0] != 0 or msg.axes[1] != 0 or msg.axes[3] != 0:
            dpos[0] = 150 * msg.axes[1]  # up to 250 mm/s
            dpos[1] = 150 * msg.axes[0]  # up to 250 mm/s
            dpos[2] = 150 * msg.axes[3]  # up to 250 mm/s

    if dsm != 0:
        jsm_pub.publish(dsm)
    elif dpos[0] != 0 or dpos[1] != 0 or dpos[2] != 0:
        jjs_pub.publish(dpos, [0.0, 0.0, 0.0])

    # publish button flags (can be handled in KR program)
    buttons = 0
    if msg.buttons[3]:
        buttons += 1
    if msg.buttons[0]:
        buttons += 2
    if msg.buttons[1]:
        buttons += 4
    if msg.buttons[2]:
        buttons += 8
    btn_pub.publish(buttons)


def main():
    global jjs_pub
    global jsm_pub
    global btn_pub
    
    rospy.init_node('ps4_control')
    
    # joint space jogging publisher
    jjs_pub = rospy.Publisher("/kr/motion/jog_linear", JogLinear, queue_size=1, tcp_nodelay=True)
    
    # self-motion jogging publisher
    jsm_pub = rospy.Publisher("/kr/motion/self_motion", SelfMotion, queue_size=1, tcp_nodelay=True)
    
    # buttons publisher
    btn_pub = rospy.Publisher("/ps4/buttons", Number, queue_size=1, tcp_nodelay=True)
    
    # subscribe joy sensor message
    rospy.Subscriber("joy", Joy, joy_cb, queue_size=1)
    
    while not rospy.is_shutdown():
        pass


if __name__ == "__main__":
    main()

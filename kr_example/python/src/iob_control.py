#!/usr/bin/env python2.7

import threading

import rospy

from kr_msgs.srv import *


def thread_spin():
    rospy.spin()


def main():
    rospy.init_node('iob_control')
    t = threading.Thread(target=thread_spin)
    t.daemon = True
    t.start()
    
    iob_set_digital_out = rospy.ServiceProxy("kr/iob/set_digital_output", SetDiscreteOutput)
    iob_set_relay_out = rospy.ServiceProxy("kr/iob/set_relay_output", SetDiscreteOutput)
    iob_set_current_out = rospy.ServiceProxy("kr/iob/set_current_output", SetAnalogOutput)
    iob_set_voltage_out = rospy.ServiceProxy("kr/iob/set_voltage_output", SetAnalogOutput)
    
    iob_get_digital_in = rospy.ServiceProxy("kr/iob/get_digital_input", GetDiscreteInput)
    iob_get_safe_in = rospy.ServiceProxy("kr/iob/get_safe_input", GetDiscreteInput)
    iob_get_current_in = rospy.ServiceProxy("kr/iob/get_current_input", GetAnalogInput)
    iob_get_voltage_in = rospy.ServiceProxy("kr/iob/get_voltage_input", GetAnalogInput)
    iob_get_quadrature_in = rospy.ServiceProxy("kr/iob/get_quadrature_input", GetQuadratureInput)
    iob_cfg_quadrature_in = rospy.ServiceProxy("kr/iob/config_quadrature_input", ConfigQuadratureInput)
    
    while not rospy.is_shutdown():
        
        print("\nSelect action:")
        print(" 1) Set digital output")
        print(" 2) Set relay output")
        print(" 3) Set voltage output")
        print(" 4) Set current output")
        print(" 5) Get digital input")
        print(" 6) Get safe input")
        print(" 7) Get current input")
        print(" 8) Get voltage input")
        print(" 9) Get quadrature input")
        print("10) Config quadrature input")
        print(" 0) Exit")
        
        output_type = int(input())
        
        if output_type == 0:
            break
        elif output_type == 1:
            print("\nEnter index (1 to 8):")
            index = int(input())
            print("\nEnter value (0 or 1):")
            value = int(input())
            print(iob_set_digital_out(index, value))
        elif output_type == 2:
            print("\nEnter index (1 to 4):")
            index = int(input())
            print("\nEnter value (0 or 1):")
            value = int(input())
            print(iob_set_relay_out(index, value))
        elif output_type == 3:
            print("\nEnter index (1 or 2):")
            index = int(input())
            print("\nEnter value (0 to 10):")
            value = float(input())
            print(iob_set_voltage_out(index, value))
        elif output_type == 4:
            print("\nEnter index (1 or 2):")
            index = int(input())
            print("\nEnter value (4 to 20):")
            value = float(input())
            print(iob_set_current_out(index, value))
        elif output_type == 5:
            print("\nEnter index (1 to 16):")
            index = int(input())
            print(iob_get_digital_in(index).value)
        elif output_type == 6:
            print("\nEnter index (1 to 4):")
            index = int(input())
            print(iob_get_safe_in(index).value)
        elif output_type == 7:
            print("\nEnter index (1 or 2):")
            index = int(input())
            print(iob_get_current_in(index).value)
        elif output_type == 8:
            print("\nEnter index (1 or 2):")
            index = int(input())
            print(iob_get_voltage_in(index).value)
        elif output_type == 9:
            print("\nEnter index (1 or 2):")
            index = int(input())
            print(iob_get_quadrature_in(index).quad)
        elif output_type == 10:
            print("\nEnter index (1 or 2):")
            index = int(input())
            print("\nEnter pulse per rev (0 to 32767):")
            ppr = int(input())
            print("\nEnter idx availability (0 or 1):")
            idx_availability = bool(input())
            print(iob_cfg_quadrature_in(index, ppr, idx_availability))
        else:
            print("Please select a valid action")
            continue


if __name__ == "__main__":
    main()
